import { showModal } from "./modal";
import { createElement } from "../helpers/domHelper";

export  function showWinnerModal(fighter) {
  const attributes = { src: fighter['source'] };
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image',  attributes});
  showModal({title: fighter.name, bodyElement: imageElement});
}