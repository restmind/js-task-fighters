import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({
    title,
    bodyElement
  });
}

function createFighterDetails(fighter) {
  const { name } = fighter;
  const attributes = { src: fighter['source'] };
  const { health } = fighter;
  const { attack } = fighter;
  const { defense } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imageElement = createElement({ tagName: 'img', className: 'fighter-image', attributes});
  const nameElement = createElement({ tagName: 'span', className: 'fighter-name' });
  const healthElement = createElement({ tagName: 'span', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'span', className: 'fighter-attack' });
  const defenseElement = createElement({ tagName: 'span', className: 'fighter-defense' });

  nameElement.innerText = "\nName: " + name + "\n";
  healthElement.innerText = "Health: " + health + "\n";
  attackElement.innerText = "Attack: " + attack + "\n";
  defenseElement.innerText = "Defense: " + defense + "\n";
  fighterDetails.append(imageElement);
  fighterDetails.append(nameElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);

  return fighterDetails;
}