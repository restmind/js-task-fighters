export function fight(firstFighter, secondFighter) {
  var healthOfFirstFighter = firstFighter.health;
  var healthOfSecondFighter = secondFighter.health;

  function isAlive() {
    return healthOfFirstFighter > 0 && healthOfSecondFighter > 0 ? true : false;
  }
  while (isAlive()) {
    healthOfFirstFighter -= getDamage(secondFighter, firstFighter);
    if (isAlive()) {
      return secondFighter;
    }

    healthOfSecondFighter -= getDamage(firstFighter, secondFighter);
    if (isAlive()) {
      return firstFighter;
    }

  }

}

export function getDamage(attacker, enemy) {
  const damage = getHitPower(attacker) - getBlockPower(enemy);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}